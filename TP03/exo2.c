#include<stdio.h>
#include<stdlib.h>


short a,b,c;

void fonction()
{
    short g,h,i;
    printf("&g: %p / &h: %p / &i: %p \n",&g,&h,&i);
}

int main(int argc, char const *argv[])
{
    short d,e,f;

    printf("&a: %p / &b: %p / &c: %p \n",&a,&b,&c);
    printf("&d: %p / &e: %p / &f: %p \n",&d,&e,&f);

    fonction();

    short g,h,i;
    printf("&g: %p / &h: %p / &i: %p \n",&g,&h,&i);


    return 0;
}


//On observe que les adresses des variables g,h,i (qui sont déclaré une deuxieme fois), changent a chaque declaration.